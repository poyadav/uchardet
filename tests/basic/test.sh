#! /bin/sh

CWD=`dirname $0`
if test x`uchardet $CWD/../tests.yml` != 'xASCII'; then
	exit 1
fi
if test x`uchardet $CWD/utf8.txt` != 'xUTF-8'; then
	exit 1
fi
